@extends('layouts.app')

@section('content')
    {{-- @include('partials.page-header') --}}


    <section class="latest-article">
        <div class="container">

            @php
                $args = [
                    'numberposts' => 1,
                ];
                
                $latest_posts = get_posts($args);
                
            @endphp

            @foreach ($latest_posts as $post)

                @include('partials.latest-article')

            @endforeach


        </div>
    </section>


    <section class="index-categories">


        @php
            $home_cats = get_field('home_categories', 'option');
        @endphp

        @if ($home_cats)
            @foreach ($home_cats as $home_cat)

                @php
                    $cat_id = $home_cat['category'];
                @endphp

                @include('partials.index-category')

            @endforeach
        @endif

    </section>



@endsection
