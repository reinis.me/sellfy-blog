@extends('layouts.app')

@section('content')


<div class="container container-narrow">

  @php
    global $wp_query;
  @endphp

  <div class="search-header">
    <h1>{!! $wp_query->found_posts !!} tag articles found</h1>

    <h2>Here's what we found for "{!! single_tag_title() !!}"</h2>
  </div>



  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-search')
  @endwhile

  <div class="pagination">
    {!! sfy_pagination() !!}

</div>

</div>

@endsection
