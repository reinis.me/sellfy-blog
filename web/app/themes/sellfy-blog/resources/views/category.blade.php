@extends('layouts.app')

@section('content')
    {{-- @include('partials.page-header') --}}
@php
          $category = get_queried_object();
@endphp
@if (get_field('show_success_stories_template', 'category_'.$category->term_id))
@include('partials.category-ss')

@else
    @include('partials.category-standard')
@endif



@endsection
