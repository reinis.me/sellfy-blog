@extends('layouts.app')

@section('content')

<div class="container container-narrow">

  @php
          
  $author_id = $post->post_author;
  $author_url = get_author_posts_url($post->post_author);
  $author_info = get_userdata($post->post_author);
  $author_name = $author_info->first_name . " " . $author_info->last_name;

@endphp

  <h1>{!! count_user_posts($author_id) !!} blog articles by</h1>

  <div class="author-card">
    <div class="author-upper">
      <div class="image" style="background-image: url({!! get_avatar_url($author_id) !!})"></div>
      <div class="author-upper-content">
        <div class="name">{{ $author_name }}</div>
        <div class="position"></div>
      </div>
    </div>
    <div class="author-lower">
      {!! the_author_meta('description') !!}
    </div>
  </div>


  @while(have_posts()) @php the_post() @endphp
    @include('partials.content-search')
  @endwhile

  <div class="pagination">
    {!! sfy_pagination() !!}

</div>

</div>

@endsection
