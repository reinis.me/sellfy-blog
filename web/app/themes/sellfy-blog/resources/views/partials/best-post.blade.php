

@php
        
$author_id = $post->post_author;
$author_url = get_author_posts_url($post->post_author);
$author_info = get_userdata($post->post_author);
$author_name = $author_info->first_name . " " . $author_info->last_name;

@endphp

<div class="best-post-item">
  <a href="{{ get_permalink($post->ID) }}" title="{{ $post->post_title }}"">
    <h3>{{ $post->post_title }}</h3>
  </a>
  <div class="best-post-author">
    <div class="image" style="background-image: url({!! get_avatar_url($author_id) !!})"></div>
    <div class="name">
      Written by
      <strong>{{ $author_name }}</strong>
    </div>
  </div>
</div>