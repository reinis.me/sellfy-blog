<div class="article-card">
  <a href="{{ get_permalink($card_post->ID) }}" title="{{ $card_post->post_title }}">
  <figure class="card-article-image">
    <div class="inner">
      @if (get_the_post_thumbnail_url($card_post->ID, 'sfy169'))
      <img src="{!! get_the_post_thumbnail_url($card_post->ID, 'sfy169') !!}" alt="{{ $card_post->post_title }}" ?>
  @endif
  
    </div>
  </a>

  </figure>
  <div class="card-article-content">
    <h1><a href="{{ get_permalink($card_post->ID) }}" title="{{ $card_post->post_title }}">{{ $card_post->post_title }}</a></h1>
    <div class="excerpt">{{ wp_trim_words(get_the_excerpt($card_post->ID), 16, false) }}</div>
  </div>

</div>
