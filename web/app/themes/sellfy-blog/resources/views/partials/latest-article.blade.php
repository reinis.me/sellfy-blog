<div class="latest-title">
  Latest article
</div>

<div class="latest-article-card">

  <a href="{{ get_permalink($post->ID) }}" title="{{ $post->post_title }}">
  <figure class="latest-article-image">
    <div class="inner">
      @if (get_the_post_thumbnail_url($post->ID, 'sfy43'))
      <img src="{!! get_the_post_thumbnail_url($post->ID, 'sfy43') !!}" alt="{{ $post->post_title }}" ?>
  @endif
  
    </div>
  </a>

  </figure>
  <div class="latest-article-content">
    <h1><a href="{{ get_permalink($post->ID) }}" title="{{ $post->post_title }}">{{ $post->post_title }}</a></h1>
    <div class="excerpt">{{ wp_trim_words(get_the_excerpt($post->ID), 16, false) }}</div>
    @php
        
      $author_id = $post->post_author;
      $author_url = get_author_posts_url($post->post_author);
      $author_info = get_userdata($post->post_author);
      $author_name = $author_info->first_name . " " . $author_info->last_name;

    @endphp
    <div class="meta">By
      <span class="author">
        <a href="{{ $author_url }}">{{ $author_name }}</a>
      </span>
      <span class="lenght">5 min read</span>
    </div>
  </div>

</div>


