@php
$category = get_queried_object();


@endphp

<div class="ss-header">
  <div class="ss-hero" style="background-image: url({!! get_field('hero_background', 'category_'.$category->term_id)["url"] !!})">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-6">
          <h1 class="hero-title">{!! get_field('ss_hero_title', 'category_'.$category->term_id) !!}</h1>
          <div class="hero-descr">{!! get_field('ss_hero_text', 'category_'.$category->term_id) !!}</div>
        </div>
        <div class="col-sm-6 col-lg-3">


        </div>
        <div class="col-sm-6 col-lg-3"></div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-6">
        <h2 class="ss-seconary-title">
          {!! get_field('ss_secondary_title', 'category_'.$category->term_id) !!}
        </h2>
        <div class="ss-seconary-descr">
          {!! get_field('ss_secondary_text', 'category_'.$category->term_id) !!}
        </div>
      </div>
    </div>
  </div>
</div>

<section class="open-category">

  <div class="container">

      <div class="row">
          <div class="col-12">
              <div class="section-title">More stories</div>

              <div class="row">

                  @php
                      $paged = get_query_var('paged') ? get_query_var('paged') : 1;
                      
                      $args = [
                          'cat' => $category->term_id,
                          'paged' => $paged,
                          // 'post__not_in' => [$latest],
                      ];
                      $query = new WP_Query($args);
                      
                  @endphp

                  @while ($query->have_posts())
                      @php
                      $query->the_post();
                          $card_post = (object) [
                              'ID' => get_the_ID(),
                              'post_title' => get_the_title(),
                          ];
                          
                      @endphp
                      <div class="col-12 col-sm-6 col-md-4">
                          @include('partials.article-card')
                      </div>
                  @endwhile
              </div>

              <div class="pagination centered">
                  {!! sfy_pagination() !!}

              </div>

          </div>

      </div>

  </div>

</section>