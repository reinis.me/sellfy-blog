<header class="page-header">
  <div class="header-upper">
    <div class="container">
      <div class="header-upper-inner">
        <a href="{{ home_url('/') }}" class="header-logo">{{ get_bloginfo('name', 'display') }}</a>
        <div class="hedaer-upper-menu-holder">
          @if (has_nav_menu('secondary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'secondary_navigation', 'menu_class' => 'nav nav-upper']) !!}
        @endif
        </div>
        <div class="header-upper-buttons">
          <button class="btn-search"></button>
          <button class="btn-menu">
            <span></span>
            <span></span>
            <span></span>
          </button>
        </div>
      </div>
    </div>
  </div>

  @if (is_home() || is_category() || is_single())


  <div class="header-lower">
    <div class="container">
      <div class="header-lower-inner">
        <div class="hedaer-lower-menu-holder">
          @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav nav-lower']) !!}
        @endif
        </div>
        <button class="btn-search"></button>
      </div>
    </div>
  </div>

      
  @endif

  @if (is_author())


  <div class="header-lower black">
    <div class="container">
      <div class="header-lower-inner">
        <div class="header-title">
          Author
        </div>
        <button class="btn-search"></button>
      </div>
    </div>
  </div>

      
  @endif

  @if (is_search())


  <div class="header-lower black">
    <div class="container">
      <div class="header-lower-inner">
        <div class="header-title">
          Search
        </div>
        <button class="btn-search"></button>
      </div>
    </div>
  </div>

      
  @endif

  @if (is_tag())


  <div class="header-lower black">
    <div class="container">
      <div class="header-lower-inner">
        <div class="header-title">
          Tag
        </div>
        <button class="btn-search"></button>
      </div>
    </div>
  </div>

      
  @endif



</header>