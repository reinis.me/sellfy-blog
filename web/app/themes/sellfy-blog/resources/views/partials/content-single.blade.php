<article @php post_class() @endphp>

  <div class="entry-content single-post-content">
    <div class="container">
      <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
          <header>
              <h1 class="entry-title">{!! get_the_title() !!}</h1>
              @include('partials/entry-meta')        
          </header>

          <div class="content-holder">
            @php the_content() @endphp

          </div>
        </div>
        <div class="col-lg-3"></div>
      </div>
      

    </div>
  </div>

</article>