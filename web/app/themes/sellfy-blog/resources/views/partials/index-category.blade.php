


<div class="container">

  <div class="index-cat-header">
    <div class="index-cat-title"><a href="{{ get_category_link($cat_id) }}">{!! get_cat_name($cat_id) !!}</a></div>
    <a href="{{ get_category_link($cat_id) }}" class="index-cat-link">See more posts</a>
  </div>

    @php
    $args = [
        'numberposts' => 4,
        'cat' => $cat_id,
    ];
    
    $latest_posts = get_posts($args);
    
@endphp



<div class="row">

  @foreach ($latest_posts as $card_post)

  <div class="col-12 col-md-6 col-lg-4 index-cat-col">
    @include('partials.article-card')
  </div>


@endforeach

</div>



  </div>


