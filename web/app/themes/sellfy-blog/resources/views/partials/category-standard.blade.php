<section class="latest-article">
  <div class="container">

      @php
          $category = get_queried_object();
          
          $args = [
              'numberposts' => 1,
              'cat' => $category->term_id,
          ];
          
          $latest_posts = get_posts($args);
          
      @endphp

      @foreach ($latest_posts as $post)

          @include('partials.latest-article')
          @php
              $latest = $post->ID;
          @endphp

      @endforeach


  </div>
</section>

<section class="open-category">

  <div class="container">

      <div class="row">
          <div class="col-12 col-lg-8">
              <div class="section-title">{!! $category->name !!}</div>

              <div class="row">

                  @php
                      $paged = get_query_var('paged') ? get_query_var('paged') : 1;
                      
                      $args = [
                          'cat' => $category->term_id,
                          'paged' => $paged,
                          'post__not_in' => [$latest],
                      ];
                      $query = new WP_Query($args);
                      
                  @endphp

                  @while ($query->have_posts())
                      @php
                      $query->the_post();
                          $card_post = (object) [
                              'ID' => get_the_ID(),
                              'post_title' => get_the_title(),
                          ];
                          
                      @endphp
                      <div class="col-12 col-sm-6">
                          @include('partials.article-card')
                      </div>
                  @endwhile
              </div>

              <div class="pagination">
                  {!! sfy_pagination() !!}

              </div>

          </div>
          <div class="col-12 col-lg-4 col-cat-sidebar">
              <div class="section-title">Best from our blog</div>

              @php
                  
                  $args = [
                      'numberposts' => 3,
                      'tag' => 'top-post',
                  ];
                  
                  $latest_posts = get_posts($args);
                  
              @endphp

              @foreach ($latest_posts as $post)

                  @include('partials.best-post')

              @endforeach

              <div class="section-title small">Topics related</div>

<div class="sidebar-tags">
@foreach (get_tags() as $tag)


<a href="{!! get_tag_link( $tag->term_id ) !!}" class="tag-link" title="{!! $tag->name !!}">{!! $tag->name !!}</a>

@endforeach
</div>

          </div>
      </div>

  </div>

</section>