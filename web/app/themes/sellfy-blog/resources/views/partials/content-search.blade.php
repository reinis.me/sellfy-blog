<div class="search-article-card">

  <a href="{!! the_permalink() !!}" title="{!! the_title() !!}">
    <figure class="search-article-image">
      <div class="inner">
        @if (get_the_post_thumbnail_url(get_the_ID(), 'sfy43'))
        <img src="{!! get_the_post_thumbnail_url(get_the_ID(), 'sfy43') !!}" alt="{!! the_title() !!}" ?>
    @endif
    
      </div>
    </a>
  
    </figure>
    <div class="search-article-content">
      <h2><a href="{!! the_permalink() !!}" title="{!! the_title() !!}">{!! the_title() !!}</a></h2>
      <div class="excerpt">{{ wp_trim_words(get_the_excerpt(), 24, false) }}</div>
      @php
          
        $author_id = $post->post_author;
        $author_url = get_author_posts_url($post->post_author);
        $author_info = get_userdata($post->post_author);
        $author_name = $author_info->first_name . " " . $author_info->last_name;
  
      @endphp
      <div class="meta">By
        <span class="author">
          <a href="{{ $author_url }}">{{ $author_name }}</a>
        </span>
        <span class="lenght">5 min read</span>
      </div>
    </div>
    

</div>