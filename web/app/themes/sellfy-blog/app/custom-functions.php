<?php 

add_action( 'after_setup_theme', 'register_menus' );

function register_menus() {
  register_nav_menu( 'secondary_navigation', __( 'Secondary navigation', 'sfy' ) );
}


function alter_menu($items, $args)
{


  // loop
  foreach ($items as &$item) {

		$link_style = get_field('link_style', $item);

      if ($link_style) {
        $item->classes[] = 'link-style-btn';
     }
    }



  // return
  return $items;
}

add_filter('wp_nav_menu_objects', 'alter_menu', 10, 2);




function pre($stuff)
{
  echo "<pre>";
  print_r($stuff);
  echo "</pre>";
}





function wpse_setup_theme() {
  add_theme_support( 'post-thumbnails' );
  add_image_size( 'sfy169', 600, 338, array( 'center', 'center' ) );
  add_image_size( 'sfy-3-2', 600, 400,  array( 'center', 'center' ) );
  add_image_size( 'sfy43', 600, 450,  array( 'center', 'center' ) );
  add_image_size( 'sfy-1-1', 600, 600,  array( 'center', 'center' ) );
}

add_action( 'after_setup_theme', 'wpse_setup_theme' );


function sfy_pagination() {
  global $wp_query;
    $big = 999999999; // need an unlikely integer
      echo paginate_links( array(
      'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
      'format' => '?paged=%#%',
      'current' => max( 1, get_query_var('paged') ),
      'total' => $wp_query->max_num_pages,
      'prev_text' => 'Back',
      'next_text' => 'Next',
      'mid_size' => 1,
      'end_size' => 1
    ) );
}
$supports = array(
  'title', // post title
  'custom-fields', // custom fields
);


$banner_labels = array(
  'name' => _x('Banners', 'plural', 'al'),
  'singular_name' => _x('Banner', 'singular', 'al'),
  'menu_name' => _x('Banners', 'admin menu', 'al'),
  'name_admin_bar' => _x('Banners', 'admin bar', 'al'),
  'add_new' => _x('Add new', 'add new', 'al'),
  'add_new_item' => __('Add new Banner', 'al'),
  'new_item' => __('New Banner', 'al'),
  'edit_item' => __('Edit Banner', 'al'),
  'view_item' => __('View Banner', 'al'),
  'all_items' => __('All Banners', 'al'),
  'search_items' => __('Search Banners', 'al'),
  'not_found' => __('No Banners found.', 'al'),
);

$banner_args = array(
  'supports' => $supports,
  'show_in_rest' => true,
  'labels' => $banner_labels,
  'public' => true,
  'query_var' => true,
  'menu_icon' => 'dashicons-align-full-width',
  'rewrite' => array('slug' => 'banner'),
  'has_archive' => true,
  'hierarchical' => true,
);

register_post_type('banner', $banner_args);

function my_remove_wp_seo_meta_box() {
	remove_meta_box('wpseo_meta', 'banner', 'normal');
	remove_meta_box('generate_select_page_header_meta_box', 'banner', 'normal');
}
add_action('add_meta_boxes', 'my_remove_wp_seo_meta_box', 100);